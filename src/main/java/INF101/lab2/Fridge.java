package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private final int totalSize;
    private int itemsInFridge;
    private List<FridgeItem> content;

    public Fridge() {
        this(20);
    }

    public Fridge(int totalSize) {
        this.totalSize = totalSize;
        itemsInFridge = 0;
        content = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return itemsInFridge;
    }

    @Override
    public int totalSize() {
        return totalSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge < totalSize) {
            content.add(item);
            itemsInFridge++;
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (content.contains(item)) {
            content.remove(item);
            itemsInFridge--;
        } else {
            throw new NoSuchElementException("Not in fridge!");
        }
    }

    @Override
    public void emptyFridge() {
        content.clear();
        itemsInFridge = 0;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        List<FridgeItem> foodToKeep = new ArrayList<>();

        for (FridgeItem item : content){
            if (item.hasExpired()){
                expiredFood.add(item);
            } else {
                foodToKeep.add(item);
            }
        }
        content = foodToKeep;
        itemsInFridge = content.size();
        return expiredFood;
    }
}
